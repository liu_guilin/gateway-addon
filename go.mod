module addon

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/json-iterator/go v1.1.10
	github.com/xiam/to v0.0.0-20200126224905-d60d31e03561
)
